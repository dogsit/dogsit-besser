// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyDYEE8yFYrU-RoMVtoHVI23ltURFQ06l28",
    authDomain: "dogsit-besser.firebaseapp.com",
    projectId: "dogsit-besser",
    storageBucket: "dogsit-besser.appspot.com",
    messagingSenderId: "129962326886",
    appId: "1:129962326886:web:f0d9245c63e11d3f37a6a7",
    measurementId: "G-83807DT62H"
    }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
