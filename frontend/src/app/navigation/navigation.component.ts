import { Component } from '@angular/core';
import { BreakpointObserver, Breakpoints, BreakpointState, } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Inserat } from '../Inserat';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],

})
export class NavigationComponent {
  inserat:Inserat[];
  isHandset$: Observable<boolean> = this.breakpointObserver
    .observe(Breakpoints.Handset)
    .pipe(
      map((result) => result.matches),
      shareReplay()
    );

    
  get isAuthenticated() {
    return this.user !== null;
  }

  get email() {
    if (this.user) return this.user.email;
    return '';
  }

  user: firebase.default.User | null = null;

  

  constructor(
    private auth: AngularFireAuth,
    private snackbar: MatSnackBar,
    private breakpointObserver: BreakpointObserver,
    private router: Router
  ) {
    this.auth.authState.subscribe((user) => {
      this.user = user;
    });
  }

  
  logout() {
    this.auth.signOut().then(() => {
      this.snackbar.open('Bye!', 'Close', {
        duration: 2500,
      });
      this.router.navigate(['/login']);
    });
  }
}
