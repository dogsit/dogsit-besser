import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavigationComponent } from './navigation/navigation.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { AuthModule } from './auth/auth.module';
import { AngularFireModule } from '@angular/fire';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { PERSISTENCE } from '@angular/fire/auth';
import { environment } from 'src/environments/environment';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { InseratelisteComponent } from './inserateliste/inserateliste.component';
import { InseratedetailComponent } from './inseratedetail/inseratedetail.component';
import { InseraterstellenComponent } from './inseraterstellen/inseraterstellen.component';
import { ReactiveFormsModule } from '@angular/forms';//forms inseraterstellen bk
import { MatInputModule } from '@angular/material/input';//forms inseraterstellen bk
import { MatSelectModule } from '@angular/material/select';//forms inseraterstellen bk
import {MatChipsModule} from '@angular/material/chips';//forms inseraterstellen bk
import {MatCheckboxModule} from '@angular/material/checkbox';
import { InseratService } from './inserat.service'; // service importieren
import { FormsModule } from '@angular/forms';
import { AngularFireStorageModule, BUCKET } from '@angular/fire/storage';
import { HomescreenComponent } from './homescreen/homescreen.component';

import { HttpClientModule } from '@angular/common/http';


@NgModule({
  declarations: [AppComponent, NavigationComponent, InseratelisteComponent, InseratedetailComponent, InseraterstellenComponent, HomescreenComponent],
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule,
    AuthModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    LayoutModule,
    MatSnackBarModule,
    MatToolbarModule,
    MatButtonModule,
    MatSidenavModule,
    MatIconModule,
    MatListModule,
    ReactiveFormsModule,
    MatInputModule,
    MatSelectModule,
    MatChipsModule,
    MatCheckboxModule,
    FormsModule,
    AngularFireStorageModule,
    HttpClientModule
  ],
  providers: [InseratService,{ provide: PERSISTENCE, useValue: 'local' },{ provide: BUCKET, useValue: 'dogsit-besser.appspot.com' }],
  bootstrap: [AppComponent],
})
export class AppModule {}
