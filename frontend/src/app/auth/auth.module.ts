import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { MatInputModule } from '@angular/material/input';
import { MatButtonModule } from '@angular/material/button';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { MatCardModule } from '@angular/material/card';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { ReactiveFormsModule } from '@angular/forms';
import { SettingsComponent } from './settings/settings.component';
import { RegisterComponent } from './login/register/register.component';
import { MatDialogModule } from '@angular/material/dialog';
import { DialogUserDataComponent } from './settings/dialog-user-data/dialog-user-data.component';

@NgModule({
  declarations: [
    LoginComponent,
    SettingsComponent,
    RegisterComponent,
    DialogUserDataComponent,
  ],
  imports: [
    CommonModule,
    MatInputModule,
    MatButtonModule,
    MatSelectModule,
    MatRadioModule,
    MatCardModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatSnackBarModule,
  ],
  exports: [LoginComponent],
})
export class AuthModule {}
