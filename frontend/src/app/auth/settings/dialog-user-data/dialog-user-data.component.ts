import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { UserData } from 'src/app/user-data';

@Component({
  selector: 'dialog-user-data',
  templateUrl: './dialog-user-data.component.html',
  styleUrls: ['./dialog-user-data.component.css'],
})
export class DialogUserDataComponent implements OnInit {
  userdataForm = this.fb.group({
    vorname: [this.userdata.vorname, Validators.required],
    nachname: [this.userdata.nachname],
    mailadr: [this.userdata.mailadr],
    wohnadr:[this.userdata.wohnadr],
    wohnort: [this.userdata.wohnort],
    postlz: [this.userdata.postlz],
    wohnregion: [this.userdata.wohnregion],
  });

  constructor(
    private fb: FormBuilder,
    private dialogRef: MatDialogRef<DialogUserDataComponent>,
    @Inject(MAT_DIALOG_DATA) public userdata: UserData
  ) {
    this.dialogRef.beforeClosed().subscribe(() => {
      this.submitUserdata();
    });
  }

  ngOnInit(): void {}

  isFormValid() {
    return this.userdataForm.valid;
  }

  closeDialog() {
    this.dialogRef.close();
  }

  private submitUserdata() {
    this.userdata.vorname = this.userdataForm.value.vorname;
    this.userdata.nachname = this.userdataForm.value.nachname;
    this.userdata.mailadr = this.userdataForm.value.mailadr;
    this.userdata.wohnadr = this.userdataForm.value.wohnadr;
    this.userdata.wohnort = this.userdataForm.value.wohnort;
    this.userdata.postlz = this.userdataForm.value.postlz;
    this.userdata.wohnregion = this.userdataForm.value.wohnregion;
  }
}
