import { Component, OnInit } from '@angular/core';
import { InseratService } from '../inserat.service';
import { Inserat } from '../Inserat';
import { MatDialog } from '@angular/material/dialog';

@Component({
  selector: 'app-inserateliste',
  templateUrl: './inserateliste.component.html',
  styleUrls: ['./inserateliste.component.css']
})
export class InseratelisteComponent implements OnInit {

  inserate : Inserat[];
  selectedIns?: Inserat;
  selected = 'Alle Tage';
  

  constructor(private inseratService : InseratService,){}


  ngOnInit(){
    this.inseratService.getInserate().subscribe( inserate => {
      this.inserate = inserate; // this.inserate also oben in export class = inserate aus subscribe als array also loopbar
    });
    
}
onSelect(inserat: Inserat):void{
  this.selectedIns=inserat;
}



filter(key:string, value:string) {
  // Methode für alle Filter, jeweils key und value anpassen
  this.inseratService.getInserate().subscribe( inserate => {
    this.inserate = inserate.filter((inserate) => inserate[key] == value); 
  });
}


filteralle() {
  // Methode um alle Inserate anzuzeigen
  this.inseratService.getInserate().subscribe( inserate => {
      this.inserate = inserate; // this.inserate also oben in export class = inserate aus subscribe als array also loopbar
    });
  }

}

