import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InseratelisteComponent } from './inserateliste.component';

describe('InseratelisteComponent', () => {
  let component: InseratelisteComponent;
  let fixture: ComponentFixture<InseratelisteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InseratelisteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InseratelisteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
