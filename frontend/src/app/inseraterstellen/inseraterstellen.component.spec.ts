import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InseraterstellenComponent } from './inseraterstellen.component';

describe('InseraterstellenComponent', () => {
  let component: InseraterstellenComponent;
  let fixture: ComponentFixture<InseraterstellenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InseraterstellenComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InseraterstellenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
