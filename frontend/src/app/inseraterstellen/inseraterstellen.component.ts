import { Component, Inject,OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { AngularFireStorage, AngularFireUploadTask } from '@angular/fire/storage';
import { FormControl} from '@angular/forms';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserData } from 'src/app/user-data';
import { InseratService } from '../inserat.service';
import { finalize } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-inseraterstellen',
  templateUrl: './inseraterstellen.component.html',
  styleUrls: ['./inseraterstellen.component.css']
})
export class InseraterstellenComponent implements OnInit {

  private userId: string | undefined;
  private userdataDoc: AngularFirestoreDocument<UserData> | undefined;

   userdata: UserData | undefined;

  insForm = this.fb.group({ //schema welches eigentliche Felder generiert
        namehu:['', Validators.required],
        alterhu:'',
        rassehu:'',
        sozhu:'',
        allerghu:'',
        groesse:'',
        nachnameha: '',
        vornameha:'',
        mailha:['',Validators.compose([Validators.required, Validators.email])],
        plz:'',
        ort: '',
        strasse: '',
        datum:'',
        tag:'', 
        zeit:'',
        angebot:'',
        region: '',
        dll:['', Validators.required],
      });
      
  disableSelect = new FormControl(false);
 // dl = this.afs.collection('inserate').add(this.downloadableURL)
 
  

  loading = false;//status asynchron deshalb statusobjekte
  success = false;

  

  constructor(
    private fb: FormBuilder, 
    private afs: AngularFirestore, 
    private http:HttpClient, 
    private storage: AngularFireStorage,
    private auth: AngularFireAuth, 
    private inseratservice:InseratService) {
      this.auth.authState.subscribe((user) => {
        if (!user) return;
        console.log(user.uid);
        this.userId = user.uid;
        this.subscribeToUserdata();
      });

    
  }
  basePath = '/Hundefotos';         
  downloadableURL = '';       
  task: AngularFireUploadTask;
  uploadPercent: Observable<number>;
  downloadURL: Observable<string>;

  

  uploadFile(event) {
    const file = event.target.files[0];
    const filePath = `${this.basePath}/${file.id}`;//wichtig sonst wird bildname als ref genommen
    const fileRef = this.storage.ref(filePath);
    const task = this.storage.upload(filePath, file);

    this.uploadPercent = task.percentageChanges();//Prozent anzeigen
    task.snapshotChanges().pipe(
        finalize(() => {this.downloadURL = fileRef.getDownloadURL()
        this.downloadURL.subscribe(url => this.downloadableURL = url)} )
     )
    .subscribe(res => {console.log(res)})
    
  }
  
  ngOnInit():void {}

  isRegistrationValid(): boolean {
    return this.insForm.valid;
  }

  async submitHandler(){
    this.loading = true;//nutzer klickt
    

    const formValue = this.insForm.value; //Wert holen der eingabe insForm
    formValue.dll = this.downloadableURL;
    console.log(formValue)

    try{
      await this.afs.collection('inserate').add(formValue);
      this.success = true //wenn ok
      this.insForm.controls['dll'].setValue(this.downloadURL);
    } catch(err){
      console.error(err)//Bei Fehler 
    }

    this.loading = false; //zurück auf Falsch weil geladen danach
  }

  private subscribeToUserdata() {
    this.userdataDoc = this.afs.doc<UserData>(`user-data/${this.userId}`);
    this.userdataDoc.valueChanges().subscribe((userdata) => {
      if (userdata){this.userdata = userdata;}
      else {
        this.userdata = {vorname: '', nachname: '',mailadr:'',postlz: '',wohnadr:'',wohnort:'',wohnregion:''
      }
      }
    });
    
  }
  
}


