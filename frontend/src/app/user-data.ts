export interface UserData {
  // Nutzerdaten erstellen in register -> settings
  // Nutzerdaten in inseraterstellen anzeigen lassen und in klasse Inserat überschreiben
  vorname: string;
  nachname: string;
  mailadr: string;
  wohnadr: string;
  wohnort: string;
  postlz: string;
  wohnregion: string;
}
