import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { SettingsComponent } from './auth/settings/settings.component';
import { InseratedetailComponent } from './inseratedetail/inseratedetail.component';
import { InseratelisteComponent } from './inserateliste/inserateliste.component';
import { InseraterstellenComponent } from './inseraterstellen/inseraterstellen.component';
import { HomescreenComponent } from './homescreen/homescreen.component';


const routes: Routes = [
  {
    path:'',
    component:HomescreenComponent,
  },
  {
    path: 'inseraterstellen',
    component: InseraterstellenComponent,
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'inserateliste',
    component: InseratelisteComponent,
  },
  {
    path: 'settings',
    component: SettingsComponent,
  },
  {
    path: 'inseratedetail/:namehu', //Pfad zum jeweiligen Detail des inserates gemäss dem Hundenamen
    component: InseratedetailComponent,
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
