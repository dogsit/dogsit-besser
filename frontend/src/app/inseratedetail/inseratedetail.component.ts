import { Component, OnInit,Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Inserat } from '../Inserat';
import { InseratService } from '../inserat.service';

@Component({
  selector: 'app-inseratedetail',
  templateUrl: './inseratedetail.component.html',
  styleUrls: ['./inseratedetail.component.css']
})
export class InseratedetailComponent implements OnInit {
inserat: Inserat;
hundename:string;
  constructor(private inserateservice: InseratService,
    private route: ActivatedRoute) {
      const hundename = this.route.snapshot.paramMap.get("namehu")
      this.hundename=hundename
      this.inserateservice.getInserate().subscribe((inserate) => {this.inserat = inserate.find((inserate) => inserate.namehu == hundename)})
     }

  ngOnInit(): void {
  }

}
