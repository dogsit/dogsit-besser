import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InseratedetailComponent } from './inseratedetail.component';

describe('InseratedetailComponent', () => {
  let component: InseratedetailComponent;
  let fixture: ComponentFixture<InseratedetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InseratedetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InseratedetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
