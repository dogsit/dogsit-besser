// service für alle interaktionen mit firebase
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFirestoreCollection } from '@angular/fire/firestore';
import { AngularFirestoreDocument } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Inserat } from './Inserat';

@Injectable({
  providedIn: 'root'
})
export class InseratService {
  insSammlung: AngularFirestoreCollection<Inserat>; // typ in interface definiert
  inserate: Observable<Inserat[]>;//Inserat initialisiert
  insDoc: AngularFirestoreDocument<Inserat>;// einzelnes Dok
  collection: any;

  constructor(private db:AngularFirestore) { // injecten inserate aus export class
   this.inserate = this.db.collection('inserate', ref =>
   ref.orderBy('namehu','asc')).valueChanges();// inserate collection gesettet und sortiert alphabetisch
    
  }

  getInserate(){
    return this.inserate//ausgeben
  }
  addInserat(inserat: Inserat){
    this.insSammlung.add(inserat);
  }

  deleteInserat(inserat: Inserat){
    this.insDoc = this.db.doc(`inserate/${inserat.id}`);
    this.insDoc.delete();
  }

  updateInserat(inserat: Inserat){
    this.insDoc = this.db.doc(`inserate/${inserat.id}`);
    this.insDoc.update(inserat);
  }
  

}

