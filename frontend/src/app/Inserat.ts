export interface Inserat {
  // Klasse Inserat - in "inseraterstellen" Variablen erstellen und
  // in Inserateliste und Inseratedetail anzeigen lassen
  id?:string,
  namehu?:string,
  alterhu?:number,
  rassehu?:string,
  sozhu?:string,
  allerghu?:string,
  groesse?:string,
  nachnameha?:string,
  vornameha?:string,
  mailha?:string,
  plz?:number,
  ort?:string,
  strasse?:string,
  datum?:number,
  tag?:string, 
  zeit?:number,
  angebot?:number,
  region?: string,
  dll?:string,
}
